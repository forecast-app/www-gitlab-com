prices:
  free_plan:
    title: Free
    edition_description: For personal projects or small teams.
    support: Unlimited private projects and collaborators
    monthly: $0
    description:
      2,000 CI pipeline minutes per month on our shared runners
    features: Community Edition <a href="/products/#comparison">features</a>*
    link: https://gitlab.com/users/sign_in
    link_text: Sign Up
  bronze_plan:
    title: Bronze
    edition_description: For personal projects or small teams who need professional support.
    paid: true
    monthly: $4
    yearly: $48
    description:
      2,000 CI pipeline minutes per month on our shared runners
    features: Enterprise Edition Starter <a href="/products/#comparison">features</a>*
    link: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff5a840412015aa3cde86f2ba6
    link_text: Buy Now
  silver_plan:
    title: Silver
    edition_description: For teams that need more advanced DevOps capabilities.
    paid: true
    monthly: $19
    yearly: $228
    description:
      10,000 CI pipeline minutes per month on our shared runners
    features: Enterprise Edition Premium <a href="/products/#comparison">features</a>*
    link: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fd5a840403015aa6d9ea2c46d6
    link_text: Buy Now
  gold_plan:
    title: Gold
    edition_description: For large enterprises who want to align strategy and execution with security and compliance.
    paid: true
    monthly: $99
    yearly: $1188
    description:
      50,000 CI pipeline minutes per month on our shared runners and a 4-hour Support SLA.
    features: Enterprise Edition Premium <a href="/products/#comparison">features</a>*
    link: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fc5a83f01d015aa6db83c45aac
    link_text: Buy Now
questions:
  - question: What are pipeline minutes?
    answer:
      Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners will not increase your pipeline minutes count and is unlimited.
  - question: What happens if I reach my minutes limit?
    answer:
      If you reach your limits, you simply won’t be able to use our shared runners to execute pipelines until the end of your current billing cycle, or until you upgrade your account to Silver or Gold. Your own runners can still be used even if you reach your limits.
  - question: Does the minute limit apply to all runners?
    answer:
      No. We will only restrict your minutes for our shared runners. If you have a <a href="https://docs.gitlab.com/runner/">specific runner setup for your projects</a>, there is no limit to your build time on GitLab.com.
  - question: Do limits apply to public and private projects?
    answer:
      The minutes limit only applies to private projects. Public projects include projects set to "Internal" as they are visible to everyone on GitLab.com.
  - question: Is there a catch with the free forever plan?
    answer:
      There is no catch. Part of <a href="https://about.gitlab.com/strategy/#sequence">our strategy sequence</a> is to make GitLab.com the most popular SaaS solution for private and public repositories. To achieve this goal you get unlimited public and private projects, and there is no limit to the number of collaborators on a project.
  - question: Can I acquire a mix of licenses?
    answer:
      No, all users in the group need to be on the same plan.
  - question: Are GitLab Pages included in the free plan?
    answer:
      Absolutely, GitLab Pages will remain free for everyone.
  - question: How do I subscribe?
    answer:
      Head over to <a href="https://customers.gitlab.com">https://customers.gitlab.com</a>, choose the plan that is right for you. After purchase, we’ll take care of upgrading your account to the plan you’ve chosen.
  - question: Can I import my projects from another provider?
    answer:
      Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket. <a href="https://docs.gitlab.com/ee/workflow/importing/README.html">See our documentation</a> for all your import options.
  - question: I already have an account, how do I upgrade?
    answer:
      Head over to <a href="https://customers.gitlab.com">https://customers.gitlab.com</a>, choose the plan that is right for you.
  - question: What about your availability and security?
    answer:
      GitLab.com is monitored 24/7. Our servers are hosted on Amazon Web Services (AWS), Digital Ocean, and Azure, we use configuration management, and we patch our servers at least once a week. Our <a href="https://gitlab.com/gitlab-com/runbooks">runbooks are public</a> as is <a href="https://gitlab.com/gitlab-com/infrastructure/issues">our operational issue tracker</a>. GitLab offers Two-Factor Authentication (2FA) via a mobile application or a U2F device, rate limiting, audit logs, and passwords are one-way encrypted. Answers to other common security questions are available on our security page.
  - question: Can I export my data?
    answer:
      You can <a href="https://docs.gitlab.com/ee/user/project/settings/import_export.html">export most of your data</a> at any time. Your data belongs to you. You are never stuck on GitLab.com, you can always export and import your project to a self hosted version of GitLab.
  - question: Do plans increase the minutes limit depending on the number of users in that group?
    answer:
      No. The limit will be applied to a group, no matter the number of users in that group.
  - question: What counts towards the disk space?
    answer:
      The project and wiki repository, Git LFS files, attachments, build artifacts, and images in the container registry.
  - question: Where can I find detailed information on GitLab.com's settings, such as SSH host keys and its shared Runners?
    answer:
      Where possible, GitLab.com uses the [standard package defaults](https://docs.gitlab.com/omnibus/package-information/defaults.html).  A list of all customized settings like the SSH host keys, Runners and Pages settings is available on the [GitLab.com settings](/gitlab-com/settings/) page.
  - question: Is GitLab.com functioning OK?
    answer:
      For more information see our status page at [status.gitlab.com](https://status.gitlab.com/) and follow [@gitlabstatus](https://twitter.com/gitlabstatus) on twitter.
  - question: Can I buy additional storage space for myself or my organisation?
    answer: >
      Not yet, but we are <a href="https://gitlab.com/gitlab-org/gitlab-ce/issues/30297#note_32311261" target="_blank">working on it</a>, you will soon be able to track your storage usage across all features and buy additional storage space for GitLab.com.
