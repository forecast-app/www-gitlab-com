---
layout: markdown_page
title: "Distribution Team Triage"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common links

* [Engineering Team Triage](https://about.gitlab.com/handbook/engineering/issue-triage/)

## Triaging Issues

Triaging issues involves investigating and applying labels and milestones to issues.

Issues filter: https://gitlab.com/gitlab-org/omnibus-gitlab/issues?assignee_id=0&milestone_title=No+Milestone&scope=all&sort=created_date&state=opened

Issues that need triaging are considered as follows:
  - They have No Milestone
  - They have No Author assigned
  - They do **not** have the following labels applied:
    * awaiting feedback
    * for scheduling

Issues are considered **partially** triaged if they have been assigned `for scheduling` or `awaiting feedback` labels.

Issues are **fully** triaged when they have been assigned a Milestone, even if it is `Backlog`

An issue that has been assigned to a user, but has no milestone, is not triaged, but is considered the responsibility of that user, and is not part of our triage queue at this time.
