---
layout: job_page
title: "Support Management"
---

## Support Management

Like other Managers, Support Managers at GitLab see the team as their product. While they know how to communicate with customers their time is spent hiring a world-class team and putting them in the best position to succeed. They own the customer support experience and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Services Support Manager

As the Services Support Manager, you will be responsible for making sure that GitLab.com customers and users receive an excellent support experience. GitLab.com is a product at scale. You’ll be hiring a global support team to support a Million+ user product with customers all over the world. While doing this you’ll be working closely with Support Engineers, GitLab.com Production, and Product teams to make sure we are listening to our customers and fixing problems faster and even before they become problems. A desire to build efficient process while maintaining a friendly and helpful customer tone is essential. If you are excited about support and want to work in a team that values collaboration and results, this is the place for you.

- Hire a world class team of Service Support Agents by actively seeking and hiring globally distributed talent who are focused on delivering world class Customer support.
- Help Services Support Specialists level up in their skills and expertise
- Build efficient process to handle volume at scale
- Hold Regular 1:1s with all Members on their team
- Exquisite communication: Regularly achieve consensus amongst their team
- Train Services Support Specialists to screen applicants and conduct interviews
- Improve the Customer Experience in measurable and repeatable ways
- Create a sense of psychological safety on their team
- Develop onboarding and training process for new Service Support Agents.
- Fully capable and excited to step into the role of a [Support Engineer](https://about.gitlab.com/roles/engineering/support-engineer/) when needed.
- Develop and implement data-driven tactics to deliver on the strategic goals for Support, as set out on the overall team's [strategy](https://about.gitlab.com/strategy/) page as well as outlined in the [direction for Support](https://about.gitlab.com/handbook/support/#support-direction).
- Develop and use analytics to measure and improve the Services Support team
- Provide mentorship as needed to improve performance and enjoyment.
- Construct and monitor metrics to maintain customer and user SLA's
- Coordinate via GitLab Issues with other teams to drive GitLab.com feature development.

#### Requirements

* 3 years or more experience in Managing people
* Experience with Zendesk or a comparable ticketing system
* Above average knowledge of SaaS Architecture and supporting such infrastructure
* Experience with ELK logging infrastructure
* Experience supporting and advocating for a Million+ User base on a SaaS product.
* Experience with debugging Modern MVC Applications and git
* Affinity for (and experience with) providing customer support
* Excellent spoken and written English
* You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values
* [A technical interview](https://about.gitlab.com/handbook/hiring/interviewing/technical/) is part of the hiring process for this position.
* A customer scenario interview is part of the hiring process for this position.
* Successful completion of a [background check](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#background-checks).

#### Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

* Qualified applicants receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 90 minute technical interview with customer scenarios with a Support Engineer
* Candidates will then be invited to schedule a 45 minute interview with our Support Manager
* Candidates will then be invited to schedule an additional 45 minute interview with the VP of Engineering
* Finally, candidates may be asked to interview with the CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

### Support Engineering Manager

- Hire a world class team of Support Engineers who are focused on delivering world class Technical support
- Help Support Engineers level up in their skills and experience
- Drive team members to be self-sufficient
- Build processes that enable team members to collaborate and execute
- Hold Regular 1:1s with all Members on their team
- Create a sense of psychological safety on your team
- Exquisite communication: Regularly achieve consensus amongst their team
- Train Support Engineers to screen applicants and conduct technical interviews
- Improve the Customer Experience in measurable and repeatable ways

### Director of Support

The Director of Support role extends the Support Engineering Manager Role

- Hire a world class team of managers and Support Engineers to work on their teams
- Help their managers and developers grow their skills and experience
- Manage multiple teams and projects
- Hold regular skip-level 1:1's with all members of their team
- Create a sense of psychological safety on their teams
- Drive technical and process improvements
- Draft and Report quarterly OKRs
- Own the support experience across all products
- Exquisite communication: Regularly achieve consensus amongst departments
- Represent the company publicly at conferences
