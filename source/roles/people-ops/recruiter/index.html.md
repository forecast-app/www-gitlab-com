---
layout: job_page
title: "Recruiter"
---

GitLab is looking for a recruiter to create positive experiences for GitLab applicants and hiring teams. We're growing quickly and need a dynamic team member to help us identify amazing candidates, improve our existing hiring practices, and deliver exceptional customer service. GitLab strives to be a preferred employer and will rely on the recruiters to act as brand ambassadors by embodying the company values and identifying those values in potential team members.  We need a recruiter who is enthusiastic about working with high volume and is dedicated to helping us build a qualified, diverse, and motivated team.

## Responsibilities

* Collaborate with managers to understand requirements and establish effective recruiting strategies
* Develop and advertise accurate job descriptions to attract a highly qualified candidate pool
* Identify creative and strategic ways to source great people
* Apply effective recruiting practices to passive and active candidates
* Source, Screen, interview and evaluate candidates
* Assess candidate interest and ability to thrive in an open source culture
* Foster lasting relationships with candidates
* Share best practice interviewing techniques with managers
* Build an effective network of internal and external resources to call on as needed
* Ensure applicants receive timely, thoughtful and engaging messaging throughout the hiring process
* Partner with Marketing to develop and deliver a disruptive employer brand strategy
* Promote our values, culture and remote only passion
* Distribute thoughtful and engaging employer brand content
* Design and monitor key metrics to evaluate the effectiveness of our employment practices
* Develop recommendations for course corrections by leveraging data from our ATS, post interview and post hire surveys and other feedback loops
* Continually search for opportunities to elevate our brand by identifying industry best practices, evaluating competitors and nurturing networks and partnerships

## Requirements

* Experience recruiting at all levels, preferably in a global capacity within the software industry, open source experience is a plus
* Proven success in recruiting and filling technical positions
* Demonstrated ability to effectively source and place candidates for all positions at all levels
* Experience with competitive global job markets preferred
* Focused on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Remote working experience in a technology startup will be an added advantage
* Ability to build relationships with managers and colleagues across multiple disciplines and timezones
* Working knowledge using an applicant tracking systems
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* College / University degree in Marketing, Human Resources or related field from an accredited institution preferred
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Intern

### Responsibilities

* Understand the roles we need filled and the requirements for those roles.
* Develop candidate reports for key roles, outlining candidates that we should be targeting for our roles.
* Act as point-of-contact for candidate inquiries regarding the application process, available positions and other recruitment-related inquiries.
* Assist recruiters with sourcing/researching activities, utilizing a variety of resources, including Search Engines and Social Networking Media, to proactively and continually source, develop and maintain an effective pipeline.
* Assist in maintaining our internal database, continuously updating candidate records.
* Perform additional tasks, projects and responsibilities as assigned. This will include assisting GitLab in building an internship program with a strong diversity focus.

### About you

* Minimum one (1) year of work experience, demonstrating responsibility and reliability (experience supporting recruiting staff preferred).
* Enrolled at an accredited University with a formal internship/co-op program.
* Comfortable using technology
* Experience working with Google Suite preferred.
* Ability to interpret substantial amounts of data.
* Excellent written & verbal communication skills both in person and over the phone/computer.
* Strong organizational skills, ability to take on multiple tasks and function under stressful situations and deadlines.
* Strong attention to detail and ability to maintain detail focus in a fast-paced environment.

## Junior

Junior recruiters share the same requirements outlined above, but typically join with less or alternate experience in one of the key areas of expertise (global experience, applicant tracking systems, technical recruiting experience, etc). Junior recruiters will be expected to:

1. Managing inbound applicant traffic for all assigned roles
1. Ensure positive candidate experiences for every applicant
1. Provide clear, thorough, and timely communications/feedback to applicants and hiring teams.
1. Identify top-tier candidates through resumes, cover letters, and initial calls.
1. Meet with hiring managers to collect job requirements and expectations
1. Write effective job descriptions

## Executive

Executive recruiters share the same requirements outlined above, but will focus on the outbound sourcing, interviewing, and closing of Director and Executive level candidates. Additional requirements for the Executive recruiters are:

1. Demonstrated history of successfully placing C-level candidates
1. A strong and active network of technology leaders in the Bay Area and beyond
1. Experience as an internal Executive business partner
1. Experience sourcing for technical/software companies strongly preferred
1. Ability to master and articulate the company and product value propositions
1. Spoken at conferences or other examples of thought-leadership
1. Live in the Bay-Area (San Francisco preferred)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our global recruiters
* A 45 minute interview with our Chief Culture officer
* A 30 minute interview with one of our People Operations Team members
* Two 45 minute interviews with one of our hiring managers

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
